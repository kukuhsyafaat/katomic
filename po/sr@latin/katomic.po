# Translation of katomic.po into Serbian.
# Strahinja Radic <rstraxy@sezampro.yu>, 2000.
# Chusslove Illich <caslav.ilic@gmx.net>, 2003, 2009, 2010, 2016.
# Bojan Bozovic <bole89@infosky.net>, 2003.
# Slobodan Simic <slsimic@gmail.com>, 2005, 2009.
# Dalibor Djuric <dalibor.djuric@mozilla-srbija.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: katomic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-05-11 03:19+0200\n"
"PO-Revision-Date: 2016-03-20 15:25+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@latin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Strahinja Radić,Marko Rosić,Miloš Puzović,Časlav Ilić,Slobodan Simić"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""
"rstraxy@sezampro.yu,roske@kde.org.yu,m.puzovic@etoncollege.org.uk,"
"chaslav@sezampro.yu,slsimic@gmail.com"

#: chooselevelsetdialog.cpp:42
#, kde-format
msgid "Level Sets"
msgstr "Skupovi nivoa"

#: gamewidget.cpp:95
#, kde-format
msgid ""
"Failed to load level set \"%1\". Check if it is installed on your computer."
msgstr "Ne mogu da učitam skup nivoa „%1“. Proverite da li je instaliran."

#: gamewidget.cpp:151
#, kde-format
msgid "Level %1 finished. "
msgstr "Nivo %1 je završen. "

#: gamewidget.cpp:155
#, kde-format
msgid "Congratulations! You have a new highscore!"
msgstr "Čestitamo! Ostvarili ste novi najrezultat!"

#. i18n: ectx: label, entry (AnimationSpeed), group (Game)
#: katomic.kcfg:9
#, kde-format
msgid "The speed of the animations."
msgstr "Brzina animacija."

#. i18n: ectx: label, entry (SavedBackgroundWidth), group (General)
#: katomic.kcfg:20
#, kde-format
msgid "Width of saved background"
msgstr "Širina sačuvane pozadine"

#. i18n: ectx: label, entry (SavedBackgroundHeight), group (General)
#: katomic.kcfg:24
#, kde-format
msgid "Height of saved background"
msgstr "Visina sačuvane pozadine"

#. i18n: ectx: label, entry (LastPlayedLevelSet), group (General)
#: katomic.kcfg:28
#, kde-format
msgid "Last played levelset"
msgstr "Poslednje igrani skup nivoa"

#. i18n: ectx: Menu (settings)
#: katomicui.rc:16
#, kde-format
msgid "&Settings"
msgstr "&Podešavanje"

#. i18n: ectx: ToolBar (mainToolBar)
#: katomicui.rc:25
#, kde-format
msgid "Main Toolbar"
msgstr "Glavna traka"

#: levelset.cpp:282
#, kde-format
msgid "Noname"
msgstr "Bezimeni"

#: levelsetdelegate.cpp:95
#, kde-format
msgid "by %1"
msgstr "%1"

#: levelsetdelegate.cpp:101
#, kde-format
msgid ", contains 1 level"
msgid_plural ", contains %1 levels"
msgstr[0] ", sadrži %1 nivo"
msgstr[1] ", sadrži %1 nivoa"
msgstr[2] ", sadrži %1 nivoa"
msgstr[3] ", sadrži %1 nivo"

#. i18n: ectx: property (text), widget (QLabel, label)
#: levelsetwidget.ui:20
#, kde-format
msgid "Choose a level set to play:"
msgstr "Izaberite skup nivoa za igranje:"

#. i18n: ectx: property (text), widget (KNS3::Button, m_pbNewStuff)
#: levelsetwidget.ui:45
#, kde-format
msgid "Get new levels"
msgstr "Dobavi nove nivoe"

#: main.cpp:39
#, kde-format
msgid "KDE Atomic Entertainment Game"
msgstr "Igra sa atomima za KDE"

#: main.cpp:56
#, kde-format
msgid "KAtomic"
msgstr "K‑atomika"

#: main.cpp:58
#, kde-format
msgid ""
"(c) 1998, Andreas Wuest\n"
"(c) 2007-2009 Dmitry Suzdalev"
msgstr ""
"© 1998, Andreas Vist\n"
"© 2007–2009, Dmitrij Suzdaljev"

#: main.cpp:59
#, kde-format
msgid "Andreas Wuest"
msgstr "Andreas Vist"

#: main.cpp:59
#, kde-format
msgid "Original author"
msgstr "Prvobitni autor"

#: main.cpp:60
#, kde-format
msgid "Dmitry Suzdalev"
msgstr "Dmitrij Suzdaljev"

#: main.cpp:60
#, kde-format
msgid "Porting to KDE4. Current maintainer"
msgstr "Prenošenje na KDE4, trenutni održavalac"

#: main.cpp:61
#, kde-format
msgid "Stephan Kulow"
msgstr "Štefan Kulov"

#: main.cpp:62
#, kde-format
msgid "Cristian Tibirna"
msgstr "Kristijan Tibirna"

#: main.cpp:63
#, kde-format
msgid "Carsten Pfeiffer"
msgstr "Karsten Pfajfer"

#: main.cpp:64
#, kde-format
msgid "Dave Corrie"
msgstr "Dejv Kori"

#: main.cpp:65
#, kde-format
msgid "Kai Jung"
msgstr "Kai Jung"

#: main.cpp:65
#, kde-format
msgid "6 new levels"
msgstr "6 novih nivoa"

#: main.cpp:66
#, kde-format
msgid "Danny Allen"
msgstr "Deni Alen"

#: main.cpp:66
#, kde-format
msgid "Game graphics and application icon"
msgstr "Grafika igre i ikonica programa"

#: main.cpp:67
#, kde-format
msgid "Johann Ollivier Lapeyre"
msgstr "Johan Olivje Laper"

#: main.cpp:67
#, kde-format
msgid "New great SVG artwork for KDE4"
msgstr "Sjajna nova SVG grafika za KDE4"

#: main.cpp:68
#, kde-format
msgid "Brian Croom"
msgstr "Brajan Krum"

#: main.cpp:68
#, kde-format
msgid "Port to use KGameRenderer"
msgstr "Prenos na KGameRenderer"

#: main.cpp:74
#, kde-format
msgid "Enable access to all levels"
msgstr "Uključi pristup svim nivoima"

#: main.cpp:85
#, kde-format
msgid ""
"KAtomic failed to find its default level set and will quit. Please check "
"your installation."
msgstr ""
"K‑atomika ne može da nađe podrazumevani skup nivoa, i zato će biti napušten. "
"Proverite instalaciju."

# @info:status
#: toplevel.cpp:55
#, kde-format
msgid "Level:"
msgstr "nivo:"

# @info:status
#: toplevel.cpp:56
#, kde-format
msgid "Current score:"
msgstr "trenutni rezultat:"

# @info:status
#: toplevel.cpp:57
#, kde-format
msgid "Highscore:"
msgstr "najrezultat:"

#: toplevel.cpp:104
#, kde-format
msgid "Previous Level"
msgstr "Prethodni nivo"

#: toplevel.cpp:111
#, kde-format
msgid "Next Level"
msgstr "Sledeći nivo"

#: toplevel.cpp:117
#, kde-format
msgid "Choose level set..."
msgstr "Skup nivoa..."

#: toplevel.cpp:121
#, kde-format
msgid "Animation Speed"
msgstr "Brzina animacije"

# >> @item:inlistbox
#: toplevel.cpp:124
#, kde-format
msgid "Slow"
msgstr "spora"

# >> @item:inlistbox
#: toplevel.cpp:124
#, kde-format
msgid "Normal"
msgstr "normalna"

# >> @item:inlistbox
#: toplevel.cpp:124
#, kde-format
msgid "Fast"
msgstr "brza"

#: toplevel.cpp:130
#, kde-format
msgid "Undo All"
msgstr "Opozovi sve"

#: toplevel.cpp:135
#, kde-format
msgid "Redo All"
msgstr "Ponovi sve"

#: toplevel.cpp:149
#, kde-format
msgid "Atom Up"
msgstr "Atom nagore"

#: toplevel.cpp:155
#, kde-format
msgid "Atom Down"
msgstr "Atom nadole"

#: toplevel.cpp:161
#, kde-format
msgid "Atom Left"
msgstr "Atom ulevo"

#: toplevel.cpp:167
#, kde-format
msgid "Atom Right"
msgstr "Atom udesno"

#: toplevel.cpp:173
#, kde-format
msgid "Next Atom"
msgstr "Sledeći atom"

#: toplevel.cpp:179
#, kde-format
msgid "Previous Atom"
msgstr "Prethodni atom"

# @info:status
#: toplevel.cpp:200
#, kde-format
msgid "Level: %1 (%2)"
msgstr "nivo: %1 (%2)"

# @info:status
#: toplevel.cpp:201
#, kde-format
msgid "Current score: %1"
msgstr "trenutni rezultat: %1"

# @info:status
#: toplevel.cpp:207
#, kde-format
msgid "Highscore: %1"
msgstr "najrezultat: %1"
