# translation of katomic.po to Slovak
# Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz>, 2000-2002.
# Stanislav Visnovsky <stano@ms.mff.cuni.cz>, 2002.
# Stanislav Visnovsky <visnovsky@kde.org>, 2003.
# Richard Fric <Richard.Fric@kdemail.net>, 2006, 2009.
# Michal Sulek <misurel@gmail.com>, 2010, 2011.
# Roman Paholik <wizzardsk@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: katomic\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-27 00:55+0000\n"
"PO-Revision-Date: 2021-11-15 17:40+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Miroslav Krajcovic,Stanislav Višňovský,Michal Šulek"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "krajcovic@hotmail.com,visnovsky@kde.org,misurel@gmail.com"

#: chooselevelsetdialog.cpp:29
#, kde-format
msgid "Level Sets"
msgstr "Sady úrovní"

#: gamewidget.cpp:84
#, kde-format
msgid ""
"Failed to load level set \"%1\". Check if it is installed on your computer."
msgstr ""
"Nepodarilo sa načítať sadu úrovne \"%1\". Skontrolujte, či je nainštalovaná "
"v počítači."

#: gamewidget.cpp:140
#, kde-format
msgid "Level %1 finished. "
msgstr "Úroveň %1 dokončená. "

#: gamewidget.cpp:144
#, kde-format
msgid "Congratulations! You have a new highscore!"
msgstr "Gratulujeme! Dosiahli ste nové najvyššie skóre!"

#. i18n: ectx: label, entry (AnimationSpeed), group (Game)
#: katomic.kcfg:9
#, kde-format
msgid "The speed of the animations."
msgstr "Rýchlosť animácií."

#. i18n: ectx: label, entry (SavedBackgroundWidth), group (General)
#: katomic.kcfg:20
#, kde-format
msgid "Width of saved background"
msgstr "Šírka uloženého pozadia"

#. i18n: ectx: label, entry (SavedBackgroundHeight), group (General)
#: katomic.kcfg:24
#, kde-format
msgid "Height of saved background"
msgstr "Výška uloženého pozadia"

#. i18n: ectx: label, entry (LastPlayedLevelSet), group (General)
#: katomic.kcfg:28
#, kde-format
msgid "Last played levelset"
msgstr "Naposledy hraná sada úrovní"

#. i18n: ectx: Menu (settings)
#: katomicui.rc:16
#, kde-format
msgid "&Settings"
msgstr "Na&stavenie"

#. i18n: ectx: ToolBar (mainToolBar)
#: katomicui.rc:25
#, kde-format
msgid "Main Toolbar"
msgstr "Hlavný panel nástrojov"

#: levelset.cpp:264
#, kde-format
msgid "Noname"
msgstr "Nepomenované"

#: levelsetdelegate.cpp:81
#, kde-format
msgid "by %1"
msgstr "od %1"

#: levelsetdelegate.cpp:87
#, kde-format
msgid ", contains 1 level"
msgid_plural ", contains %1 levels"
msgstr[0] ", obsahuje 1 úroveň"
msgstr[1] ", obsahuje %1 úrovne"
msgstr[2] ", obsahuje %1 úrovní"

#. i18n: ectx: property (text), widget (QLabel, label)
#: levelsetwidget.ui:20
#, kde-format
msgid "Choose a level set to play:"
msgstr "Vyberte sadu úrovní hry:"

#. i18n: ectx: property (text), widget (KNSWidgets::Button, m_pbNewStuff)
#: levelsetwidget.ui:45
#, kde-format
msgid "Get new levels"
msgstr "Získať nové úrovne"

#: main.cpp:44
#, kde-format
msgid "KAtomic"
msgstr "KAtomic"

#: main.cpp:46
#, kde-format
msgid "Atomic Entertainment Game"
msgstr "Zábavná hra s atómami pre KDE"

#: main.cpp:48
#, kde-format
msgid ""
"(c) 1998, Andreas Wuest\n"
"(c) 2007-2009 Dmitry Suzdalev"
msgstr ""
"(c) 1998, Andreas Wuest\n"
"(c) 2007-2009 Dmitry Suzdalev"

#: main.cpp:49
#, kde-format
msgid "Andreas Wuest"
msgstr "Andreas Wuest"

#: main.cpp:49
#, kde-format
msgid "Original author"
msgstr "Pôvodný autor"

#: main.cpp:50
#, kde-format
msgid "Dmitry Suzdalev"
msgstr "Dmitry Suzdalev"

#: main.cpp:50
#, kde-format
msgid "Porting to KDE4. Current maintainer"
msgstr "Prenos do KDE4. Aktuálny správca"

#: main.cpp:51
#, kde-format
msgid "Stephan Kulow"
msgstr "Stephan Kulow"

#: main.cpp:52
#, kde-format
msgid "Cristian Tibirna"
msgstr "Cristian Tibirna"

#: main.cpp:53
#, kde-format
msgid "Carsten Pfeiffer"
msgstr "Carsten Pfeiffer"

#: main.cpp:54
#, kde-format
msgid "Dave Corrie"
msgstr "Dave Corrie"

#: main.cpp:55
#, kde-format
msgid "Kai Jung"
msgstr "Kai Jung"

#: main.cpp:55
#, kde-format
msgid "6 new levels"
msgstr "6 nových úrovní"

#: main.cpp:56
#, kde-format
msgid "Danny Allen"
msgstr "Danny Allen"

#: main.cpp:56
#, kde-format
msgid "Game graphics and application icon"
msgstr "Grafika hry a ikona aplikácie"

#: main.cpp:57
#, kde-format
msgid "Johann Ollivier Lapeyre"
msgstr "Johann Ollivier Lapeyre"

#: main.cpp:57
#, kde-format
msgid "New great SVG artwork for KDE4"
msgstr "Nová skvelá SVG grafika pre KDE4"

#: main.cpp:58
#, kde-format
msgid "Brian Croom"
msgstr "Brian Croom"

#: main.cpp:58
#, kde-format
msgid "Port to use KGameRenderer"
msgstr "Portovanie na KGameRenderer"

#: main.cpp:64
#, kde-format
msgid "Enable access to all levels"
msgstr "Povoliť prístup do všetkých úrovní"

#: main.cpp:75
#, kde-format
msgid ""
"KAtomic failed to find its default level set and will quit. Please check "
"your installation."
msgstr ""
"KAtomic nemohol nájsť štandardnú sadu úrovní a preto sa ukončí. Prosím, "
"skontrolujte inštaláciu."

#: toplevel.cpp:41
#, kde-format
msgid "Level:"
msgstr "Úroveň:"

#: toplevel.cpp:42
#, kde-format
msgid "Current score:"
msgstr "Aktuálne skóre:"

#: toplevel.cpp:43
#, kde-format
msgid "Highscore:"
msgstr "Najvyššie skóre:"

#: toplevel.cpp:90
#, kde-format
msgid "Previous Level"
msgstr "Predchádzajúca úroveň"

#: toplevel.cpp:97
#, kde-format
msgid "Next Level"
msgstr "Nasledujúca úroveň"

#: toplevel.cpp:103
#, kde-format
msgid "Choose level set..."
msgstr "Vyberte sadu úrovní..."

#: toplevel.cpp:107
#, kde-format
msgid "Animation Speed"
msgstr "Rýchlosť animácie"

#: toplevel.cpp:110
#, kde-format
msgid "Slow"
msgstr "Pomalá"

#: toplevel.cpp:110
#, kde-format
msgid "Normal"
msgstr "Normálna"

#: toplevel.cpp:110
#, kde-format
msgid "Fast"
msgstr "Rýchla"

#: toplevel.cpp:117
#, kde-format
msgid "Undo All"
msgstr "Všetko späť"

#: toplevel.cpp:122
#, kde-format
msgid "Redo All"
msgstr "Všetko znovu"

#: toplevel.cpp:136
#, kde-format
msgid "Atom Up"
msgstr "Atóm hore"

#: toplevel.cpp:142
#, kde-format
msgid "Atom Down"
msgstr "Atóm dole"

#: toplevel.cpp:148
#, kde-format
msgid "Atom Left"
msgstr "Atóm doľava"

#: toplevel.cpp:154
#, kde-format
msgid "Atom Right"
msgstr "Atóm doprava"

#: toplevel.cpp:160
#, kde-format
msgid "Next Atom"
msgstr "Nasledujúci atóm"

#: toplevel.cpp:166
#, kde-format
msgid "Previous Atom"
msgstr "Predchádzajúci atóm"

#: toplevel.cpp:192
#, kde-format
msgid "Level: %1 (%2)"
msgstr "Úroveň: %1 (%2)"

#: toplevel.cpp:193
#, kde-format
msgid "Current score: %1"
msgstr "Aktuálne skóre: %1"

#: toplevel.cpp:199
#, kde-format
msgid "Highscore: %1"
msgstr "Najvyššie skóre: %1"
